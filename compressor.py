import numpy as np
import math

class Compressor:
    dT = 100
    def __init__(self, raw, applyRound = True):
        self.raw = raw
        self.applyRound = applyRound

    def angle_compressor(self):
        angleCompressedEcg = np.zeros(len(self.raw))
        angleCompressedEcg[0] = self.raw[0] #ref
        prec = angleCompressedEcg[0]
        for i in range(1, len(self.raw)):
            next = self.raw[i]
            angleRad = (math.pi/2) + math.atan((next-prec)/self.dT) #così sono sempre positivi perchè i punti sono o nel 1° o nel 4° quadrante.
            angleByte = self.__round__(255 * (angleRad / math.pi)) #simulo l'appossimazione di salvare in 1 byte
            angleCompressedEcg[i] = angleByte
            prec = next
            pass
        return angleCompressedEcg

    def angle_compressor_with_compensation(self):
        angleCompressedEcg = np.zeros(len(self.raw))
        angleCompressedEcg[0] = self.raw[0] #ref
        prec = angleCompressedEcg[0]
        for i in range(1, len(self.raw)):
            next = self.raw[i]
            angleRad = (math.pi/2) + math.atan((next-prec)/self.dT) #così sono sempre positivi perchè i punti sono o nel 1° o nel 4° quadrante.
            angleByte = self.__round__(255 * (angleRad / math.pi)) #simulo l'appossimazione di salvare in 1 byte
            angleCompressedEcg[i] = angleByte
            #Compensazione
            prec += math.tan((math.pi * angleByte / 255) - (math.pi/2))*self.dT
            pass
        return angleCompressedEcg

    def angle_decompressor(self, cmpr):
        ecg = np.zeros(len(cmpr))
        ecg[0] = cmpr[0] #ref
        prec = ecg[0]
        for i in range(1, len(cmpr)):
            angleRad = (math.pi * cmpr[i] / 255) - (math.pi/2)
            dY = math.tan(angleRad)*self.dT
            ecg[i] = prec + dY
            prec = ecg[i]
        return ecg

    def vector_compressor(self):
        vectorCompressedEcg = np.zeros(len(self.raw))
        for i in range(0, len(self.raw)):
            next = self.raw[i]
            vectorRad = (math.pi/2) + math.atan(next/self.dT) #così sono sempre positivi perchè i punti sono o nel 1° o nel 4° quadrante.
            vectorByte = self.__round__(255 * (vectorRad / math.pi)) #simulo l'appossimazione di salvare in 1 byte
            vectorCompressedEcg[i] = vectorByte
            pass
        return vectorCompressedEcg

    def vector_decompressor(self, cmpr):
        ecg = np.zeros(len(cmpr))
        for i in range(0, len(cmpr)):
            angleRad = (math.pi * cmpr[i] / 255) - (math.pi/2)
            dY = math.tan(angleRad)*self.dT
            if dY<-2000:
                dY=-2000
            if dY>2000:
                dY=2000
            ecg[i] = dY
        return ecg

    def __round__(self, n):
        if self.applyRound: return round(n)
        return n
