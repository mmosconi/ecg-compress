#!/usr/bin/python3
import struct
import numpy as np

class EcgParser:
    file = None
    pos = 0
    total = 0

    def __init__(self, filename):
        self.filename = filename
        self.file = open(filename, 'rb')

    def readTotalSeconds(self):
        ishne  = self.__read_str__(8)
        crc = self.__read_uint16__()
        print ( ishne, "CRC=", crc)
        variableLen = self.__read_uint32__()
        ecgLen = self.__read_uint32__()
        print ("Variable bytes =", variableLen)
        print ("ECG seconds =", ecgLen/128)
        self.__skip__((512-self.pos)+variableLen)
        self.total = ecgLen
        return ecgLen/128

    def read(self, fromS, deltaS):
        self.__skip__(128*fromS*2)
        reducedLen = min(deltaS*128, self.total-self.pos);
        ecg = np.zeros(reducedLen)
        for i in range(0, reducedLen):
            ecg[i] = self.__read_int16__()
        return ecg

    def __read_str__(self, len):
        self.pos+=len;
        return str(self.file.read(len))

    def __read_uint32__(self):
        self.pos+=4;
        return struct.unpack('<I', self.file.read(4))[0]

    def __read_uint16__(self):
        self.pos+=2;
        return struct.unpack('<H', self.file.read(2))[0]

    def __read_int16__(self):
        self.pos+=2;
        return struct.unpack('<h', self.file.read(2))[0]

    def __skip__(self, bytes):
        self.file.read(bytes)
        self.pos+=bytes
        pass
