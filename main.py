#!/usr/bin/python3
from ecgParser import EcgParser
from compressor import Compressor
import matplotlib.pyplot as plt
import numpy as np
import math

def error(raw, dec):
    delta = np.absolute(np.subtract(raw, dec))
    minErr = np.amin(delta)
    maxErr = np.amax(delta)
    avgErr = np.average(delta)
    stnDev = np.std(delta)
    return {"min": minErr, "max": maxErr, "avg": avgErr, "std": stnDev}

parser = EcgParser("./data/short.ecg")
tot = parser.readTotalSeconds()

raw = parser.read(16, 30)
x = np.arange(0, len(raw), 1)

testCompressor = Compressor(raw, applyRound=False)
angleTestDecompressed = testCompressor.angle_decompressor(testCompressor.angle_compressor())
print ("Angle no compression", str(error(raw, angleTestDecompressed)))
vectorTestDecompressed = testCompressor.vector_decompressor(testCompressor.vector_compressor())
print ("Vector no compression", str(error(raw, vectorTestDecompressed)))
compressor = Compressor(raw)
angleDecompressed = compressor.angle_decompressor(compressor.angle_compressor())
print ("Angle compression", str(error(raw, angleDecompressed)))
angleDecompressedCompensated = compressor.angle_decompressor(compressor.angle_compressor_with_compensation())
print ("Angle compression with compensation", str(error(raw, angleDecompressedCompensated)))
vectorDecompressed = compressor.vector_decompressor(compressor.vector_compressor())
print ("Vector compression", str(error(raw, vectorDecompressed)))

plt.figure(1)

plt.subplot(3, 3, 1)
plt.plot(x, raw)

plt.subplot(3, 3, 2)
plt.plot(x, raw, 'b', x, angleTestDecompressed, 'r')

plt.subplot(3, 3, 3)
plt.plot(x, raw, 'b', x, vectorTestDecompressed, 'g')

plt.subplot(3, 3, 5)
plt.plot(x, raw, 'b', x, angleDecompressed, 'r')
plt.subplot(3, 3, 8)
plt.plot(x, raw, 'b', x, angleDecompressedCompensated, 'r')
plt.subplot(3, 3, 6)
plt.plot(x, raw, 'b', x, vectorDecompressed, 'g')


plt.show()
